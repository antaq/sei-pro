
// ADICIONA ACOMPANHAMENTO DE PROCESSOS
function getOptionsConfigDate(index) {
    var storeFavorites = getStoreFavoritePro();
    var configdate = (!$.isEmptyObject(storeFavorites['favorites'][index]['configdate'])) 
                        ? storeFavorites['favorites'][index]['configdate']
                        : {
                            date: moment().format('YYYY-MM-DD'), 
                            listdocs: false,
                            dateDue: moment().add(5,'d').format('YYYY-MM-DD'), 
                            countdown: true, 
                            countdays: false, 
                            workday: false, 
                            setdate: true, 
                            duenumber: 5, 
                            duecounter: 'corrido', 
                            duemode: 'depois', 
                            duesetdate: false, 
                            duedate: false, 
                            newdoc: true,
                            selectdoc: false,
                            advanced: false,
                            displayformat: false,
                            displayicon: false,
                            displaydue: false,
                            displaydue_txt: 'Vencimento:',
                            displaytip: '',
                            deliverydoc: false,
                            deliverydoc_style: '',
                            newdoclist: []
                        };
    return configdate;
}
function openBoxConfigDates(this_) {
    var index = parseInt($(this_).closest('tr').data('index'));
    var storeFavorites = getStoreFavoritePro();
    var dateInput = $(this_).closest('.info_dates_fav_txt').find('.favoriteDatesPro').val();
    var date_ = (dateInput == '') ? moment().format('YYYY-MM-DD') : dateInput;
    var configdate = getOptionsConfigDate(index);
        configdate.date = (dateInput == '') ? configdate.date : date_;
    
    var stateCountDown = (configdate.countdown) ? 'checked' : '';
    var stateCountDownIcon = (configdate.countdown) ? 'azulColor' : 'cinzaColor';
    var stateCountDownDiv = (configdate.countdown) ? '' : 'display:none';
    var stateSetDates = (configdate.setdate) ? 'checked' : '';
    var stateSetDatesIcon = (configdate.setdate) ? 'azulColor' : 'cinzaColor';
    var stateSetDatesDiv = (configdate.setdate) ? '' : 'display:none';
    var stateSelectDoc = (configdate.selectdoc) ? 'checked' : '';
    var stateSelectDocIcon = (configdate.selectdoc) ? 'azulColor' : 'cinzaColor';
    var stateSelectDocDiv = (configdate.selectdoc) ? '' : 'display:none';
    var stateCountDays = (configdate.countdays) ? 'checked' : '';
    var stateCountDaysIcon = (configdate.countdays) ? 'azulColor' : 'cinzaColor';
    var stateCountDaysDiv = (configdate.countdays) ? '' : 'display:none';
    var stateWorkday = (configdate.workday) ? 'checked' : '';
    var stateWorkdayIcon = (configdate.workday) ? 'azulColor' : 'cinzaColor';
    var stateWorkdayDiv = (configdate.workday) ? '' : 'display:none';
    var stateDueDate = (configdate.duedate) ? 'checked' : '';
    var stateDueDateIcon = (configdate.duedate) ? 'azulColor' : 'cinzaColor';
    var stateDueDateDiv = (configdate.duedate) ? '' : 'display:none';
    var stateDueSetDate = (configdate.duesetdate) ? 'checked' : '';
    var stateDueSetDateIcon = (configdate.duesetdate) ? 'azulColor' : 'cinzaColor';
    var stateDueSetDateDiv = (configdate.duesetdate) ? '' : 'display:none';
    var stateNewDoc = (configdate.newdoc) ? 'checked' : '';
    var stateNewDocIcon = (configdate.newdoc) ? 'azulColor' : 'cinzaColor';
    var stateNewDocDiv = (configdate.newdoc) ? '' : 'display:none';
    var stateAdvancedDiv = (configdate.advanced) ? '' : 'display:none';
    var stateAdvancedIcon = (configdate.advanced) ? 'newLink_active' : '';
    var htmlNewDocList = appendArrayNewdoclist(configdate.newdoclist);
    var stateDuecounter_corrido = (configdate.duecounter == 'corrido') ? 'selected' : '';
    var stateDuecounter_util = (configdate.duecounter == 'util') ? 'selected' : '';
    var stateDuemode_depois = (configdate.duemode == 'depois') ? 'selected' : '';
    var stateDuemode_antes = (configdate.duemode == 'antes') ? 'selected' : '';
    var duenumber = (configdate.duenumber > 0 ) ? configdate.duenumber : Math.abs(configdate.duenumber);

    var htmlBox =   '<div id="configDatesBox">'+
                    '   <table style="font-size: 10pt;width: 100%;" class="seiProForm">'+
                    '      <tr style="height: 40px;">'+
                    '          <td colspan="2">Contar o tempo decorrido do processo a partir:</td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-file-signature '+stateSelectDocIcon+'"></i> '+
                    '               Da data de assinatura de um documento'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_selectdoc" data-type="selectdoc" tabindex="0" '+stateSelectDoc+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_selectdoc"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px; '+stateSelectDocDiv+'" class="configDates_selectdoc">'+
                    '          <td colspan="2">'+
                    '               <select onchange="configDatesSetUpdate(\'update\')" id="configDatesBox_listdocs">';
        if (storeFavorites['favorites'][index]['documentos'].length > 0) {
            $.each(storeFavorites['favorites'][index]['documentos'], function(i,value){
                var selected = (configdate.listdocs && configdate.listdocs == value.id_protocolo) ? 'selected' : '';
                htmlBox +=   (value.data_assinatura == '') ? '' : '                   <option data-sign="'+value.data_assinatura+'" data-id-protocolo="'+value.id_protocolo+'" '+selected+'>'+value.documento+' (SEI n\u00BA '+value.nr_sei+') [assinado em '+value.data_assinatura+']</option>';
            });
        }
        htmlBox +=  '               </select>'+
                    '           </td>'+
                    '      </tr>'+
                    '      <tr style="height: 10px; display:none" class="configDates_selectdoc"><td colspan="2"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-calendar-check '+stateSetDatesIcon+'"></i> '+
                    '               De uma data espec\u00EDfica'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_setdate" data-type="setdate" tabindex="0" '+stateSetDates+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_setdate"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px; '+stateSetDatesDiv+'" class="configDates_setdate">'+
                    '          <td>'+
                    '               <i class="iconPopup fas fa-clock cinzaColor"></i> Data referencial'+
                    '          </td>'+
                    '          <td>'+
                    '               <input type="date" onchange="configDatesPreview()" id="configDatesBox_date" value="'+configdate.date+'" style="width:130px; float: right;">'+
                    '           </td>'+
                    '      </tr>'+
                    '      <tr style="height: 10px;">'+
                    '           <td colspan="2">'+
                    '               <a class="newLink '+stateAdvancedIcon+'" onclick="configDatesAdvanced(this)" style="font-size: 10pt; cursor: pointer; margin: 5px 0 0 0; float: right;"><i class="fas fa-wrench cinzaColor"></i> Op\u00E7\u00F5es avan\u00E7adas</a>'+
                    '           </td></tr>'+
                    '   </table>'+
                    '   <table style="font-size: 10pt; width: 100%; '+stateAdvancedDiv+'" class="seiProForm configDates_advanced">'+
                    '      <tr class="hrForm"><td colspan="4"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td colspan="2">Visualizar o resultado:</td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-stopwatch '+stateCountDownIcon+'"></i> '+
                    '               Em tempo relativo'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_countdown" data-type="countdown" tabindex="0" '+stateCountDown+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_countdown"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-calendar-day '+stateCountDaysIcon+'"></i> '+
                    '               Em n\u00FAmero de dias'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_countdays" data-type="countdays" tabindex="0" '+stateCountDays+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_countdays"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px; '+stateCountDaysDiv+'" class="configDates_countdays">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-briefcase '+stateWorkdayIcon+'"></i> '+
                    '               Calcular em dias \u00FAteis'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchIcon(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_workday" data-type="workday" tabindex="0" '+stateWorkday+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_workday"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr class="hrForm"><td colspan="4"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td colspan="2">Sinalizar a partir:</td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-pen-fancy '+stateNewDocIcon+'"></i> '+
                    '               Da assinatura de um novo documento'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_newdoc" data-type="newdoc" tabindex="0" '+stateNewDoc+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_newdoc"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr class="configDates_newdoc"><td colspan="2"><span id="configDatesBox_newdoclist">'+htmlNewDocList+'</span></td></tr>'+
                    '      <tr style="height: 40px; '+stateNewDocDiv+'" class="configDates_newdoc">'+
                    '          <td colspan="2">'+
                    '               <select id="configDatesBox_listnewdoc" onchange="configDatesDocsChange(this)">'+
                    '                   <option value="0">Qualquer tipo de documento</option>';
        if (storeFavorites['config']['tiposdocs'].length > 0) {
            $.each(storeFavorites['config']['tiposdocs'], function(i,value){
                htmlBox +=   (value.name == '') ? '' : '                   <option value="'+value.id+'" >'+value.name+'</option>';
            });
        }
        htmlBox +=  '               </select>'+
                    '           </td>'+
                    '      </tr>'+
                    '      <tr style="height: 10px;"><td colspan="2"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-hourglass-half '+stateDueDateIcon+'"></i> '+
                    '               Do n\u00FAmero de dias decorridos'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_duedate" data-type="duedate" tabindex="0" '+stateDueDate+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_duedate"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px; '+stateDueDateDiv+'" class="configDates_duedate">'+
                    '          <td colspan="2">'+
                    '               <input type="number" onchange="configDatesPreview()" id="configDatesBox_duenumber" value="'+duenumber+'" style="width:40px; margin-left: 35px !important;" min="0">'+
                    '               dias '+
                    '               <select id="configDatesBox_duecounter" onchange="configDatesPreview()" style="width: auto;">'+
                    '                   <option value="corrido" '+stateDuecounter_corrido+'>corridos</option>'+
                    '                   <option value="util" '+stateDuecounter_util+'>\u00FAteis</option></select>'+
                    '               <select id="configDatesBox_duemode" onchange="configDatesPreview()" style="width: auto;">'+
                    '                   <option value="depois" '+stateDuemode_depois+'>depois</option>'+
                    '                   <option value="antes" '+stateDuemode_antes+'>antes</option>'+
                    '               </select>'+
                    '               <span class="configDates_selectdoc" style="display:none">da data de assinatura</span>'+
                    '               <span class="configDates_setdate">da data de refer\u00EAncia</span>'+
                    '           </td>'+
                    '      </tr>'+
                    '      <tr style="height: 10px;" class="configDates_duedate"><td colspan="2"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td><i class="iconPopup iconSwitch fas fa-calendar-alt '+stateDueSetDateIcon+'"></i> '+
                    '               De uma data de vencimento espec\u00EDfica'+
                    '          </td>'+
                    '          <td>'+
                    '              <div class="onoffswitch" style="float: right;">'+
                    '                  <input type="checkbox" onchange="configDatesSwitchChange(this)" name="onoffswitch" class="onoffswitch-checkbox" id="configDatesBox_duesetdate" data-type="duesetdate" tabindex="0" '+stateDueSetDate+'>'+
                    '                  <label class="onoffswitch-label" for="configDatesBox_duesetdate"></label>'+
                    '              </div>'+
                    '          </td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px; '+stateDueSetDateDiv+'" class="configDates_duesetdate">'+
                    '          <td>'+
                    '               <i class="iconPopup iconSwitch fas fa-clock cinzaColor"></i> Data de vencimento'+
                    '          </td>'+
                    '          <td>'+
                    '               <input type="date" onchange="configDatesPreview()" id="configDatesBox_duesetdt" value="'+configdate.dateDue+'" style="width:130px; float: right;">'+
                    '           </td>'+
                    '      </tr>'+
                    '   </table>'+
                    '   <table style="font-size: 10pt;width: 100%;" class="seiProForm">'+
                    '      <tr class="hrForm"><td colspan="4"></td></tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td colspan="2">Pr\u00E9via:</td>'+
                    '      </tr>'+
                    '      <tr style="height: 40px;">'+
                    '          <td colspan="2">'+
                    '              <div id="dateboxPreview" style="display:none; text-align: center;"></div>'+
                    '          </td>'+
                    '      </tr>'+
                    '   </table>'+
                    '   <input type="hidden" value="'+index+'" id="configDatesBox_index">'+
                    '</div>';
    
        resetDialogBoxPro('dialogBoxPro');
        dialogBoxPro = $('#dialogBoxPro')
            .html('<div class="dialogBoxDiv">'+htmlBox+'</div>')
            .dialog({
                title: "Contagem de tempo decorrido",
                width: 500,
                close: function() { $('#configDatesBox').remove() },
                buttons: [{
                    text: "Ok",
                    click: function() {
                        saveConfigDatesFav();
                        $(this).dialog('close');
                    }
                }]
        });
        configDatesPreview();
}
function appendArrayNewdoclist(listArray) {
    var htmlDoc = '';
    $.each(listArray, function(i,value){
        htmlDoc += appendNewdoclist(value);
    });
    return htmlDoc;
}
function appendNewdoclist(nameDoc) {
    return  '<span class="dateboxDoc">'+
            '   <i class="far fa-file-alt" style="color: #777; padding-right: 3px;"></i> '+
                nameDoc+
            '   <i class="fas fa-times" style="color: #F783AD; padding-left: 3px; cursor:pointer" onclick="javascript:$(this).closest(\'span\').remove()"></i> '+
            '</span>';
}
function configDatesDocsChange(this_) {
    var nameDoc = $(this_).find('option:selected').text();
    var valueDoc = parseInt($(this_).val());
    var listDocsSelected = $('#configDatesBox_newdoclist').find('.dateboxDoc').map(function () {  return $(this).text().trim() }).get();
    if (valueDoc == 0) {
        $('#configDatesBox_newdoclist').html('');
    } else if (!listDocsSelected.includes(nameDoc)) {
        if (listDocsSelected.length > 10) {
            alert('Atingido o limite de documentos para pesquisa (10)');
        } else {
            var htmlDoc = appendNewdoclist(nameDoc);
                $('#configDatesBox_newdoclist').append(htmlDoc);
                //console.log(htmlDoc);
        }
    }
}
function configDatesAdvanced(this_) {
    $('.configDates_advanced').toggle();
    $(this_).toggleClass('newLink_active');
}
function configDatesSetUpdate(mode) {
    var dataSign = $('#configDatesBox_listdocs').find('option:selected').data('sign');
    if (dataSign) {
        $('#configDatesBox_date').val(moment(dataSign,'DD/MM/YYYY').format('YYYY-MM-DD'));
        if (mode == 'update') { 
            configDatesPreview(); 
        }
    }
}
function saveConfigFav() {
    var storeFavorites = getStoreFavoritePro();
    if (typeof storeFavorites !== 'undefined' && storeFavorites.hasOwnProperty('favorites')) {
        storeFavorites = jmespath.search(storeFavorites.favorites, "[*].{id_procedimento: id_procedimento, assuntos: assuntos, descricao: descricao, interessados: interessados, processo: processo, configdate: configdate}");
        if (typeof perfilLoginAtiv !== 'undefined' && perfilLoginAtiv !== null) {
            var action = 'set_favoritos';
            var param = {
                config: encodeURIComponent(encodeJSON_toHex(JSON.stringify(storeFavorites))),
                action: action
            };
            getServerAtividades(param, action);
        }
    }
}
function saveConfigDatesFav() {
    var config = getConfigDatesFav();
    var storeFavorites = getStoreFavoritePro();
    var index = parseInt($('#configDatesBox_index').val());
    if (index >=0 && typeof storeFavorites['favorites'][index] !== undefined && typeof getConfigDatesFav() !== undefined) {
        //console.log(config);
        var htmlDatePreview = getDatesPreview(config);
        var trFavorite = $('#favoritesProDiv table.tableFollow tbody').find('tr[data-index="'+index+'"]');
        //var followLink = trFavorite.find('.followLinkDates')[0].outerHTML;
        var followLink = '';
            $('#configDatesBox').remove();
            trFavorite.find('.info_dates_fav').html(htmlDatePreview+followLink).show().closest('td').find('.info_dates_fav_txt').hide().find('.favoriteDatesPro').val(config.date);
            trFavorite.find('.followLinkDatesEdit').show();
            storeFavorites['favorites'][index]['configdate'] = config;
            localStorageStorePro('configDataFavoritesPro', storeFavorites);
            alertaBoxPro('Sucess', 'check-circle', 'Contagem de tempo cadastrada com sucesso!');
            saveConfigFav();
    } else {
        alertaBoxPro('Error', 'exclamation-triangle', 'Erro ao cadastrar!');
    }
}
function getConfigDatesFav() {
    var countdown = $('#configDatesBox_countdown').is(':checked');
    var countdays = $('#configDatesBox_countdays').is(':checked');
    var workday = $('#configDatesBox_workday').is(':checked');
    var selectdoc = $('#configDatesBox_selectdoc').is(':checked');
    var duedate = $('#configDatesBox_duedate').is(':checked');
    var duesetdate = $('#configDatesBox_duesetdate').is(':checked');
    var newdoc = $('#configDatesBox_newdoc').is(':checked');
    var setdate = $('#configDatesBox_setdate').is(':checked');
    var duemode = $('#configDatesBox_duemode').val();
    var duecounter = $('#configDatesBox_duecounter').val();
    var dateDue = $('#configDatesBox_duesetdt').val();
    var duenumber = $('#configDatesBox_duenumber').val();
        duenumber = (duemode == 'depois') ? duenumber : -Math.abs(duenumber);
    var listdocs = $('#configDatesBox_listdocs').find('option:selected').data('id-protocolo');
    var date = $('#configDatesBox_date').val();
    var dateTo = moment().format('YYYY-MM-DD');
    var newdoclist = (newdoc) ? $('#configDatesBox_newdoclist').find('.dateboxDoc').map(function () {  return $(this).text().trim() }).get() : [];
    var advanced = (countdays || duedate || duesetdate || newdoclist.length > 0) ? true : false;
    return {date: date, dateDue: dateDue, advanced: advanced, newdoclist: newdoclist, listdocs: listdocs, setdate: setdate, newdoc: newdoc, countdown: countdown, countdays: countdays, workday: workday, duenumber: parseInt(duenumber), duecounter: duecounter, duemode: duemode, duesetdate: duesetdate, duedate: duedate, selectdoc: selectdoc};
}
function configDatesSwitchIcon(this_) {
    if ($(this_).is(':checked')) { 
        $(this_).closest('tr').find('.iconSwitch').addClass('azulColor');
    } else {
        $(this_).closest('tr').find('.iconSwitch').removeClass('azulColor');
    }
    configDatesPreview();
}
function configDatesSwitchChange(this_) {
    configSwitchChange(this_, 'countdown', 'countdays');
    configSwitchChange(this_, 'setdate', 'selectdoc');
    configSwitchChange(this_, 'duedate', 'newdoc', 'duesetdate');
    configDatesPreview();
    if ($('#configDatesBox_selectdoc').is(':checked')) { configDatesSetUpdate('update') }
}
function configSwitchChange(this_, option1, option2, option3) {
    var data_this = $(this_).data();
    if (data_this.type == option1 && $(this_).is(':checked')) {
        $('#configDatesBox_'+option2).prop('checked', false);
        $('#configDatesBox_'+option2).closest('tr').find('.iconSwitch').removeClass('azulColor');
        if (option3) { 
            $('#configDatesBox_'+option3).prop('checked', false); 
            $('#configDatesBox_'+option3).closest('tr').find('.iconSwitch').removeClass('azulColor');
        }
        $('.configDates_'+option1).fadeIn('slow');
        $('.configDates_'+option2).fadeOut('slow');
        if (option3) { $('.configDates_'+option3).fadeOut('slow'); }
    } else if (data_this.type == option1 && !$(this_).is(':checked')) {
        $('#configDatesBox_'+option2).prop('checked', true);
        $('#configDatesBox_'+option2).closest('tr').find('.iconSwitch').addClass('azulColor');
        if (option3) { 
            $('#configDatesBox_'+option3).prop('checked', false); 
            $('#configDatesBox_'+option3).closest('tr').find('.iconSwitch').removeClass('azulColor');
        }
        $('.configDates_'+option2).fadeIn('slow');
        $('.configDates_'+option1).fadeOut('slow');
        if (option3) { $('.configDates_'+option3).fadeOut('slow'); }
    } else if (data_this.type == option2 && !$(this_).is(':checked')) {
        $('#configDatesBox_'+option1).prop('checked', true);
        $('#configDatesBox_'+option1).closest('tr').find('.iconSwitch').addClass('azulColor');
        if (option3) { 
            $('#configDatesBox_'+option3).prop('checked', false); 
            $('#configDatesBox_'+option3).closest('tr').find('.iconSwitch').removeClass('azulColor');
        }
        $('.configDates_'+option1).fadeIn('slow');
        $('.configDates_'+option2).fadeOut('slow');
        if (option3) { $('.configDates_'+option3).fadeOut('slow'); }
    } else if (data_this.type == option2 && $(this_).is(':checked')) {
        $('#configDatesBox_'+option1).prop('checked', false);
        $('#configDatesBox_'+option1).closest('tr').find('.iconSwitch').removeClass('azulColor');
        if (option3) { 
            $('#configDatesBox_'+option3).prop('checked', false); 
            $('#configDatesBox_'+option3).closest('tr').find('.iconSwitch').removeClass('azulColor');
        }
        $('.configDates_'+option1).fadeOut('slow');
        $('.configDates_'+option2).fadeIn('slow');
        if (option3) { $('.configDates_'+option3).fadeOut('slow'); }
    } else if (option3 && data_this.type == option3 && !$(this_).is(':checked')) {
        $('#configDatesBox_'+option2).prop('checked', true);
        $('#configDatesBox_'+option2).closest('tr').find('.iconSwitch').addClass('azulColor');
        $('#configDatesBox_'+option1).prop('checked', false);
        $('#configDatesBox_'+option1).closest('tr').find('.iconSwitch').removeClass('azulColor');
        $('.configDates_'+option2).fadeIn('slow');
        $('.configDates_'+option1).fadeOut('slow');
        $('.configDates_'+option3).fadeOut('slow');
    } else if (option3 && data_this.type == option3 && $(this_).is(':checked')) {
        $('#configDatesBox_'+option1).prop('checked', false);
        $('#configDatesBox_'+option1).closest('tr').find('.iconSwitch').removeClass('azulColor');
        $('#configDatesBox_'+option2).prop('checked', false);
        $('#configDatesBox_'+option2).closest('tr').find('.iconSwitch').removeClass('azulColor');
        $('.configDates_'+option1).fadeOut('slow');
        $('.configDates_'+option2).fadeOut('slow');
        $('.configDates_'+option3).fadeIn('slow');
    }
    if ($(this_).is(':checked')) { 
        $(this_).closest('tr').find('.iconSwitch').addClass('azulColor');
    } else {
        $(this_).closest('tr').find('.iconSwitch').removeClass('azulColor');
    }
}
function updateCountTableFav() {
    var count = $('.tableFollow').find('tbody').find('tr:visible').length;
    var countTxt = (count == 1) ? count+' registro:' : count+' registros:';
        $('.tableFollow').find('caption.infraCaption').text(countTxt);
}
function getStoreFavoritePro() {
    return ( typeof localStorageRestorePro('configDataFavoritesPro') !== 'undefined' && !$.isEmptyObject(localStorageRestorePro('configDataFavoritesPro')) ) ? localStorageRestorePro('configDataFavoritesPro') : {favorites: [], config: {colortags: []} };
}
function insertIconFavorites() {
    waitLoadPro($('#ifrArvore').contents(), '#topmenu', "a[target='ifrVisualizacao']", appendIconFavorites);
}
function appendIconFavorites() {
    var ifrArvore = $('#ifrArvore').contents(); 
    var iconProc = ifrArvore.find('#topmenu a[target="ifrVisualizacao"]').eq(0);
    var id_procedimento = String(getParamsUrlPro(iconProc.attr('href')).id_procedimento);
    var storeFavorites = getStoreFavoritePro()['favorites'];
    var dataFavorites = (jmespath.search(storeFavorites, "[?id_procedimento=='"+id_procedimento+"'] | length(@)") > 0) ? jmespath.search(storeFavorites, "[?id_procedimento=='"+id_procedimento+"'] | [0]") : '';
    var iconStar = (dataFavorites == '') 
                    ? '<i title="Adicionar Processo aos Favoritos" id="iconFavoritePro" onclick="parent.actFavoritePro(\'add\')" class="far fa-star" style="font-size: 12pt; margin: 0 5px; color: #666; cursor: pointer;"></i>'
                    : '<i title="Remover Processo dos Favoritos" id="iconFavoritePro" onclick="parent.actFavoritePro(\'remove\')" class="fas fa-star starGold" style="font-size: 12pt; margin: 0 5px; cursor: pointer; -webkit-text-fill-color: #FED35B; -webkit-text-stroke-color: rgb(216 162 22); -webkit-text-stroke-width: 2px;"></i>';
    ifrArvore.find('#iconFavoritePro').remove();
    ifrArvore.find('#topmenu a[target="ifrVisualizacao"]').eq(0).after(iconStar);    
}
function actFavoritePro(mode) {
    var ifrArvore = $('#ifrArvore').contents(); 
    var iconProc = ifrArvore.find('#topmenu a[target="ifrVisualizacao"]').eq(0);
    var id_procedimento = String(getParamsUrlPro(iconProc.attr('href')).id_procedimento);
    checkDataFavoritePro(mode, id_procedimento);
}
function checkDataFavoritePro(mode, id_procedimento, TimeOut = 9000) {
    if (TimeOut <= 0) { return; }
    if (typeof dadosProcessoPro !== 'undefined' && dadosProcessoPro.hasOwnProperty('listAndamento') && dadosProcessoPro.hasOwnProperty('propProcesso') && dadosProcessoPro.hasOwnProperty('tiposDocumentos')) { 
        storeFavoritePro(mode, id_procedimento);
    } else {
        setTimeout(function(){ 
            checkDataFavoritePro(mode, id_procedimento, TimeOut - 100); 
            $('#ifrArvore').contents().find('#iconFavoritePro').fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
            console.log('Reload checkDataFavoritePro'); 
        }, 500);
    }
}
function storeFavoritePro(mode, id_procedimento) {
    if (mode == 'add') {
        var storeFavorites = addFavoritePro();
    } else {
        var storeFavorites = removeFavoritePro(id_procedimento);
    }
    if (dadosProcessoPro.tiposDocumentos.length > 0 ) { storeFavorites['config']['tiposdocs'] = dadosProcessoPro.tiposDocumentos; }
    localStorageStorePro('configDataFavoritesPro', storeFavorites);
    saveConfigFav();
    appendIconFavorites();
}
function removeFavoritePainelPro(this_, id_procedimento = 0) {
    confirmaBoxPro('Tem certeza que deseja remover esse processo dos favoritos?', function(){
        if (id_procedimento == 0) {
            $('#favoriteTablePro').find('input[name="favoritePro"]:checked').each(function(){
                var id_procedimento = $(this).val();
                removeFavoritePainelPro_(this, id_procedimento);
            });
            setTimeout(function(){ 
                $(this_).hide();
            }, 500);
        } else {
            removeFavoritePainelPro_(this_, id_procedimento);
        }
    });
}
function removeFavoritePainelPro_(this_, id_procedimento) {
    var _this = $(this_);
    var storeFavorites = removeFavoritePro(id_procedimento);
    localStorageStorePro('configDataFavoritesPro', storeFavorites);
    saveConfigFav();
    _this.closest('tr').slideUp();
}
function updateFavorites(this_) {
    $(this_).find('i').addClass('fa-spin');
    setPanelFavorites('refresh');
}
function removeFavoritePro(id_procedimento) {
    var storeFavorites = getStoreFavoritePro();
    for (i = 0; i < storeFavorites['favorites'].length; i++) {
        if( storeFavorites['favorites'][i]['id_procedimento'] == id_procedimento) {
            console.log('notinclude', i, storeFavorites['favorites'][i]['id_procedimento'], storeFavorites['favorites'][i]['processo']);
            storeFavorites['favorites'].splice(i,1);
            i--;
        }
    }
    return storeFavorites;
}
function editCategoryFavorite(this_, id_procedimento) {
    var _this = $(this_);
    var storeFavorites = getStoreFavoritePro()['favorites'];
    var category_elem = _this.closest('td').find('.info_category');
    var category_txt = _this.closest('td').find('.info_category_txt');
    if (category_elem.is(':visible')) {
        category_elem.hide();
        category_txt.show();
        _this.find('i').attr('class','fas fa-pencil-alt');
    } else {
        var value = jmespath.search(storeFavorites, "[?id_procedimento=='"+id_procedimento+"'] | [0]");
        var categoriaLista = selectCategoryFavorite(value.categoria, 'changeCategoryFavorite', true, id_procedimento);
        category_elem.show().html(categoriaLista);
        category_txt.hide();
        _this.find('i').attr('class','fas fa-thumbs-up');
    }
}
function selectCategoryFavorite(select = '', func, newItem = false, id_procedimento = 0) {
    var storeFavorites = getStoreFavoritePro()['favorites'];
    var listaCategorias = jmespath.search(storeFavorites,"[*].categoria");
        listaCategorias = (listaCategorias !== null) ? uniqPro(listaCategorias) : false;
    var categoriaLista = (listaCategorias) 
        ? $.map(listaCategorias, function(v, i){
            if (v !== null && v != '') {
                var selected = (select !== null && v == select) ? 'selected' : '';
                return '<option value="'+v+'" '+selected+'>'+v+'</option>';
            }
        }).join('')
        : '';
        categoriaLista = '<select class="selectPro" '+(id_procedimento ? 'style="margin: 0 !important;" data-id="'+id_procedimento+'"' : 'style="width: 100%;"')+' onchange="'+func+'(this)"><option value=""></option>'+categoriaLista+(newItem ? '<option value="new">\u2795 Nova categoria</option>' : '')+'</select>';
   return categoriaLista;
}
function changePanelCategoryFavorite(this_) {
    var _this = $(this_);
    setOptionsPro('panelFavoritosView', _this.val());
    setPanelFavorites('refresh');
}
function changeCategoryFavorite(this_) {
    var _this = $(this_);
    if (_this.val() == 'new') {
        var textBox =   '<i class="fas fa-info-circle azulColor" style="margin-right: 5px;"></i> Digite o nome da nova categoria:'+
                        '<br><br><span class="seiProForm" style="text-align: center; display: block; font-size: 9pt;">'+
                        '   <input onkeypress="if (event.which == 13) { $(this).closest(\'.ui-dialog\').find(\'.confirm.ui-button\').trigger(\'click\') }" type="text" style="width: 90% !important;" class="required infraText" value="" name="nomeNovoItem" id="nomeNovoItem">'+
                        '</span>';
        resetDialogBoxPro('alertBoxPro');
        alertBoxPro = $('#alertaBoxPro')
            .html('<div class="dialogBoxDiv"> '+textBox+'</span>')
            .dialog({
                width: 400,
                title: 'Adicionar nova categoria',
                close: function() {
                    _this.closest('td').find('.info_category').hide().html('');
                    _this.closest('td').find('.info_category_txt').show();
                },
                open: function() {
                    $('#nomeNovoItem').focus();
                },
                buttons: [{
                    text: "Ok",
                    class: 'confirm ui-state-active',
                    click: function() {
                        saveCategoryFavorite(this_, $('#nomeNovoItem').val());
                    }
                }]
        });
    } else {
        saveCategoryFavorite(this_, _this.val());
    }
}
function saveCategoryFavorite(this_, value) {
    var _this = $(this_);
    var data = _this.data();
    var id_procedimento = data.id;
    var storeFavorites = getStoreFavoritePro();
    var favoriteIndex = storeFavorites.favorites.findIndex((obj => obj.id_procedimento == id_procedimento));

    if (typeof favoriteIndex !== 'undefined' && favoriteIndex !== null) {
            var item = storeFavorites.favorites[favoriteIndex];
                item.categoria = value;
            storeFavorites.favorites[favoriteIndex] = item;
    }
    localStorageStorePro('configDataFavoritesPro', storeFavorites);
    saveConfigFav();
    setPanelFavorites('refresh');
    if (alertBoxPro) {   
        alertBoxPro.dialog('close');
        resetDialogBoxPro('alertBoxPro');
    }
}
function addFavoritePro() {
    var storeFavorites = getStoreFavoritePro();
        storeFavorites['favorites'].push({
            id_procedimento: dadosProcessoPro.listAndamento.id_procedimento,
            processo: dadosProcessoPro.listAndamento.processo,
            andamento: dadosProcessoPro.listAndamento.andamento,
            documentos: dadosProcessoPro.listDocumentos,
            tipo_procedimento: dadosProcessoPro.propProcesso.hdnNomeTipoProcedimento,
            assuntos: dadosProcessoPro.propProcesso.selAssuntos_select,
            interessados: dadosProcessoPro.propProcesso.selInteressadosProcedimento,
            descricao: dadosProcessoPro.propProcesso.txtDescricao,
            order: -1,
            categoria: ''
        });
        console.log('### addFavoritePro', storeFavorites); 
    return storeFavorites;
}
function setPanelFavorites(mode) {
    var statusView = ( getOptionsPro('favoritesProDiv') == 'hide' ) ? 'display:none;' : 'display: inline-table;';
    var statusIconShow = ( getOptionsPro('favoritesProDiv') == 'hide' ) ? '' : 'display:none;';
    var statusIconHide = ( getOptionsPro('favoritesProDiv') == 'hide' ) ? 'display:none;' : '';
    var storeFavorites = getStoreFavoritePro()['favorites'];
        storeFavorites = (checkObjHasProperty(storeFavorites, 'order')) ? jmespath.search(storeFavorites, "sort_by([*],&order)") : storeFavorites;
    var arrayProcessosUnidade = getProcessoUnidadePro();
    var selectedCategoryView = (getOptionsPro('panelFavoritosView')) ? getOptionsPro('panelFavoritosView') : '';

    var listFavorite = (selectedCategoryView != '') ? jmespath.search(storeFavorites, "[?categoria=='"+selectedCategoryView+"']") : storeFavorites;
    var countFavorite = (listFavorite.length == 1) ? listFavorite.length+' registro:' : listFavorite.length+' registros:';

    if (listFavorite === null || listFavorite.length == 0) {
        var htmlTableFavorites = '<div class="dataFallback" data-text="Nenhum dado dispon\u00EDvel"></div>';
    } else {
        var htmlToolbarFav =    '<div id="toolbar-options-fav" class="hidden">'+
                                '   <a href="#" data-action="etiqueta" style="width: 175px;"><i class="fa fas fa-tags"></i><span class="info" title="Editar Etiqueta" alt="Editar Etiqueta">Editar Etiqueta</span></a>'+
                                '   <a href="#" data-action="dates" style="width: 175px;"><i class="fa fa-clock"></i><span class="info" title="Contar tempo decorrido" alt="Contar tempo decorrido">Contar tempo decorrido</span></a>'+
                                '   <a href="#" data-action="descricao" style="width: 175px;"><i class="fa fa-comment-alt"></i><span class="info" title="Editar descri\u00E7\u00E3o" alt="Editar descri\u00E7\u00E3o">Editar descri\u00E7\u00E3o</span></a>'+
                                '   <a href="#" data-action="remove" style="width: 175px;"><i class="fa fa-trash"></i><span class="info" title="Remover Processo" alt="Remover Processo">Remover Processo</span></a>'+
                                '</div>';
        var htmlTableFavorites =    '<table class="tableInfo tableZebra tableFollow tableFavoritos tabelaControle" data-name-table="Favoritos" data-tabletype="favoritos" id="favoriteTablePro">'+
                                    '   <caption class="infraCaption" style="text-align: left;">'+countFavorite+'</caption>'+
                                    '   <thead>'+
                                    '       <tr class="tableHeader">'+
                                    '           <th class="tituloControle" style="width: 50px;" align="center"><label class="lblInfraCheck" for="lnkInfraCheck" accesskey=";"></label><a id="lnkInfraCheck" onclick="getSelectAllTr(this, \'SemGrupo\');"><img src="/infra_css/imagens/check.gif" id="imgRecebidosCheck" title="Selecionar Tudo" alt="Selecionar Tudo" class="infraImg"></a></th>'+
                                    '           <th class="tituloControle" style="width: 210px;">Processo</th>'+
                                    '           <th class="tituloControle tituloFilter" data-filter-type="date" style="width: 150px;">Prazo</th>'+
                                    '           <th class="tituloControle tituloFilter" data-filter-type="etiqueta" style="width: 150px;">Etiqueta</th>'+
                                    '           <th class="tituloControle">Especifica\u00E7\u00E3o</th>'+
                                    '           <th class="tituloControle">Tipo de Processo</th>'+
                                    '           <th class="tituloControle">Categoria</th>'+
                                    '           <th class="tituloControle" style="width: 50px;" align="center"><i class="fas fa-sort-numeric-up"></i></th>'+
                                    '       </tr>'+
                                    '   </thead>'+
                                    '   <tbody>';
            $.each(listFavorite,function(index, value){
                var linkDoc = url_host+'?acao=procedimento_trabalhar&id_procedimento='+value.id_procedimento;
                var tagsFav = (typeof value.etiquetas !== 'undefined') ? value.etiquetas.join(';') : '';
                var tagsFavHtml = (typeof value.etiquetas !== 'undefined') ? $.map(listFavorite[index].etiquetas, function (i) { return getHtmlEtiqueta(i,'fav') }).join('') : '';
                var tagsFavClass = (typeof value.etiquetas !== 'undefined') ? $.map(listFavorite[index].etiquetas, function (i) { return 'tagTableName_'+removeAcentos(i).replace(/\ /g, '').toLowerCase(); }).join(' ') : '';   
                var datesFav = (typeof value.configdate !== 'undefined') ? value.configdate.date : '';
                if (typeof value.configdate !== 'undefined') { value.configdate.dateTo = moment().format('YYYY-MM-DD') }
                var datesFavHtml = (typeof value.configdate !== 'undefined') ? getDatesPreview(value.configdate) : '';
                var tagDatesFavClass = (datesFavHtml != '') ? 'tagTableName_'+$(datesFavHtml).data('tagname') : '';
                var iconProcesso = ( $.inArray(value.processo, arrayProcessosUnidade) == -1 ) ? 'fas fa-folder' : 'far fa-folder-open';
                var tipsProcesso = ( $.inArray(value.processo, arrayProcessosUnidade) == -1 ) ? 'Processo fechado nesta unidade' : 'Processo aberto nesta unidade';
                var issetOrder = (value.hasOwnProperty('order') && value.order !== null && value.order != -1) ? true : false;
                var order = (issetOrder) ? value.order : index;
                var categoria = (value.hasOwnProperty('categoria') && value.categoria !== null && value.categoria != '') ? value.categoria : false;
                if (selectedCategoryView == '' || selectedCategoryView == categoria) {
                    htmlTableFavorites +=   '       <tr data-tagname="SemGrupo" data-index="'+index+'" data-id_procedimento="'+value.id_procedimento+'" class="'+tagsFavClass+' '+tagDatesFavClass+'">'+
                                            '           <td align="center"><input type="checkbox" onclick="followSelecionarItens(this)" id="favoritePro_'+value.id_procedimento+'" name="favoritePro" value="'+value.id_procedimento+'"></td>'+
                                            '           <td align="left">'+
                                            '               <a style="text-decoration: underline; color: #00c;" href="'+linkDoc+'">'+
                                            '               <i class="'+iconProcesso+'" style="color: #00c;text-decoration: underline;"  onmouseover="return infraTooltipMostrar(\''+tipsProcesso+'\');" onmouseout="return infraTooltipOcultar();"></i> '+
                                            '               '+value.processo+'</a>'+
                                            '               <a class="newLink followLink followLinkNewtab" href="'+linkDoc+'" onmouseover="return infraTooltipMostrar(\'Abrir em nova aba\');" onmouseout="return infraTooltipOcultar();" target="_blank"><i class="fas fa-external-link-alt" style="font-size: 90%; text-decoration: underline;"></i></a>'+
                                            '           </td>'+
                                            '           <td align="left" class="tdfav_dates '+((datesFavHtml.trim() == '' ) ? 'info_dates_follow_empty' : '')+'">'+
                                            '               <span class="info_dates_fav">'+datesFavHtml+
                                            '               </span>'+
                                            '               <a class="newLink followLink followLinkDates followLinkDatesEdit" onclick="showDatesFav(this, \'show\')" onmouseover="return infraTooltipMostrar(\'Editar prazo\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-pencil-alt" style="font-size: 100%;"></i></a>'+
                                            '               <a class="newLink followLink followLinkDates followLinkDatesAdd" onclick="showDatesFav(this, \'show\')" onmouseover="return infraTooltipMostrar(\'Adicionar prazo\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-stopwatch" style="font-size: 100%;"></i></a>'+
                                            '               <span class="info_dates_fav_txt" style="display:none;">'+
                                            '                   <input value="'+datesFav+'" onblur="showDatesFav(this, \'hide\')"  onkeypress="keyDatesFav(event)" type="date" class="favoriteDatesPro">'+
                                            '                   <a class="newLink" onclick="showDatesFav(this, \'hide\')" style="padding: 2px; margin: 0 2px;" onmouseover="return infraTooltipMostrar(\'Salvar\');" onmouseout="return infraTooltipOcultar();">'+
                                            '                      <i class="fas fa-thumbs-up" style="font-size: 100%;"></i>'+
                                            '                   </a>'+
                                            '                   <a class="newLink favoriteConfigDates" onclick="openBoxConfigDates(this)" style="padding: 2px; margin: 0 2px;" onmouseover="return infraTooltipMostrar(\'Configura\u00E7\u00F5es\');" onmouseout="return infraTooltipOcultar();">'+
                                            '                      <i class="fas fa-cog" style="font-size: 100%;"></i>'+
                                            '                   </a>'+
                                            '               </span>'+
                                            '           </td>'+
                                            '           <td align="left" class="tdfav_tags '+((tagsFavHtml.trim() == '' ) ? 'info_tags_follow_empty' : '')+'" data-etiqueta-mode="fav">'+
                                            '               <span class="info_tags_follow">'+tagsFavHtml+
                                            '               </span>'+
                                            '               <span class="info_tags_follow_txt" style="display:none">'+
                                            '                   <input value="'+tagsFav+'" class="favoriteTagsPro">'+
                                            '               </span>'+
                                            '               <a class="newLink followLink followLinkTags followLinkTagsEdit" onclick="showFollowEtiqueta(this, \'show\', \'fav\')" onmouseover="return infraTooltipMostrar(\'Editar etiqueta\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-pencil-alt" style="font-size: 100%;"></i></a>'+
                                            '               <a class="newLink followLink followLinkTags followLinkTagsAdd" onclick="showFollowEtiqueta(this, \'show\', \'fav\')" onmouseover="return infraTooltipMostrar(\'Adicionar etiqueta\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-tags" style="font-size: 100%;"></i></a>'+
                                            '           </td>'+
                                            '           <td class="tdfav_desc">'+
                                            '               <span class="info_txt" style="display:none"><input onblur="saveFollowDesc(this, \'fav\')" onkeypress="keyFollowDesc(event, \'fav\')" value="'+value.descricao+'"></span>'+
                                            '               <span class="info">'+value.descricao+'</span>'+
                                            '               <a class="newLink followLink followLinkDesc" onclick="editFollowDesc(this, \'fav\')" onmouseover="return infraTooltipMostrar(\'Editar especifica\u00E7\u00E3o\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-pencil-alt" style="font-size: 100%;"></i></a>'+
                                            '           </td>'+
                                            '           <td>'+
                                            '               '+value.tipo_procedimento+
                                            '               <a class="newLink followLink followLinkTags followLinkFavRemove" onclick="removeFavoritePainelPro(this, \''+value.id_procedimento+'\')" onmouseover="return infraTooltipMostrar(\'Remover favorito\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-trash-alt" style="font-size: 100%;"></i></a>'+
                                            '           </td>'+
                                            '           <td class="td_fav_category">'+
                                            '               <span class="info_category_txt">'+(categoria ? categoria : '')+'</span>'+
                                            '               <span class="info_category" style="display:none"></span>'+
                                            '               <a class="newLink followLink followLinkTags followLinkFavCategory" onclick="editCategoryFavorite(this, \''+value.id_procedimento+'\')" onmouseover="return infraTooltipMostrar(\'Editar categoria\');" onmouseout="return infraTooltipOcultar();"><i class="fas fa-pencil-alt" style="font-size: 100%;"></i></a>'+
                                            '           </td>'+
                                            '           <td align="center" data-order="'+order+'">'+
                                            '               <a class="newLink sorterTrFavorite" style="margin-right: 20px;"></i>'+
                                            '                   <span class="fa-layers fa-fw">'+
                                            '                       <i class="fas fa-bars cinzaColor"></i>'+
                                            (issetOrder ? 
                                            '                       <span class="fa-layers-counter">'+value.order+'</span>'+
                                            '' : '')+
                                            '                   </span>'+
                                            '               </a>'+
                                            '           </td>'+
                                            '       </tr>';
                }
            });
            htmlTableFavorites +=   '   </tbody>'+
                                    '</table>';
        var idOrder = (getOptionsPro('orderPanelHome') && jmespath.search(getOptionsPro('orderPanelHome'), "[?name=='favoritesPro'].index | length(@)") > 0) ? jmespath.search(getOptionsPro('orderPanelHome'), "[?name=='favoritesPro'].index | [0]") : '';
        var htmlPanelFavorites = '<div class="panelHomePro" style="display: inline-block; width: 100%;" id="favoritesPro" data-order="'+idOrder+'">'+
                                '   <div class="infraBarraLocalizacao titlePanelHome">'+
                                '       <i class="fas fa-star starGold" style="margin: 0 5px; font-size: 1.1em;"></i>'+
                                '       Favoritos'+
                                '       <a class="newLink" id="favoritesProDiv_showIcon" onclick="toggleTablePro(\'favoritesProDiv\',\'show\')" onmouseover="return infraTooltipMostrar(\'Mostrar Tabela\');" onmouseout="return infraTooltipOcultar();" style="font-size: 11pt; '+statusIconShow+'"><i class="fas fa-plus-square cinzaColor"></i></a>'+
                                '       <a class="newLink" id="favoritesProDiv_hideIcon" onclick="toggleTablePro(\'favoritesProDiv\',\'hide\')" onmouseover="return infraTooltipMostrar(\'Recolher Tabela\');" onmouseout="return infraTooltipOcultar();" style="font-size: 11pt; '+statusIconHide+'"><i class="fas fa-minus-square cinzaColor"></i></a>'+
                                '   </div>'+
                                '   <div id="favoritesProDiv" style="width: 98%; '+statusView+'">'+
                                '   	<div id="favoritosProActions" style="top:0; position: absolute; z-index: 9; left: 190px; width: calc(100% - 230px)">'+
                                '           <a class="newLink iconFavoritos_remove" onclick="removeFavoritePainelPro(this)" onmouseover="return infraTooltipMostrar(\'Remover favoritos\');" onmouseout="return infraTooltipOcultar();" style="margin: 0;font-size: 14pt; display: none">'+
                                '                   <span class="fa-layers fa-fw">'+
                                '                       <i class="fas fa-trash-alt"></i>'+
                                '                       <span class="fa-layers-counter">1</span>'+
                                '                   </span>'+
                                '           </a>'+
                                '           <span style="display:block; float:right; width:200px;">'+selectCategoryFavorite(selectedCategoryView, 'changePanelCategoryFavorite')+'</span>'+
                                '           <a class="newLink iconFavoritos_update" onclick="updateFavorites(this)" onmouseover="return infraTooltipMostrar(\'Atualizar Informa\u00E7\u00F5es\');" onmouseout="return infraTooltipOcultar();" style="margin: 0;font-size: 14pt;float: right;">'+
                                '               <i class="fas fa-sync-alt"></i>'+
                                '           </a>'+
                                '   	</div>'+
                                '   	<div class="tabelaPanelScroll">'+
                                        htmlToolbarFav+htmlTableFavorites+
                                '   	</div>'+
                                '   </div>'+
                                '</div>';
    }
    if ( mode == 'insert' ) {
        if ( $('#favoritesPro').length > 0 ) { $('#favoritesPro').remove(); }
        //$('#panelHomePro').append(htmlPanelFavorites);
        orderDivPanel(htmlPanelFavorites, idOrder, 'favoritesPro');

        if (getOptionsPro('panelSortPro')) {
            initSortDivPanel();
        }

    } else if ( mode == 'refresh' ) {
        $('#favoritesPro').attr('id', 'favoritesPro_temp');
        $('#favoritesPro_temp').after(htmlPanelFavorites);
        $('#favoritesPro_temp').remove();
    }
    initFunctionsPanelFav();
}
function keyDatesFav(e) {
    if(e.which == 13) {
        showDatesFav(e.path[0], 'hide');
    }
}
function initFunctionsPanelFav(TimeOut = 9000) {
    if (TimeOut <= 0) { return; }
    if (typeof $('.favoriteTagsPro').tagsInput !== 'undefined' && 
        typeof $('.tabelaPanelScroll').tagsInput !== 'undefined' && 
        typeof $().tablesorter !== 'undefined' && 
        typeof $('.ui-autocomplete-input').autocomplete !== 'undefined') {

        var idTableFavorite = '#favoriteTablePro';
        var tableFavorites = $(idTableFavorite);

        $('.favoriteTagsPro').tagsInput({
          interactive: true,
          placeholder: 'Adicionar etiqueta',
          minChars: 2,
          maxChars: 100,
          limit: 8,
          autocomplete_url: '',
          autocomplete: {'source': sugestEtiquetaPro('fav') },
          hide: true,
          delimiter: [';'],
          unique: true,
          removeWithBackspace: true,
          onAddTag: saveFollowEtiqueta,
          onRemoveTag: saveFollowEtiqueta,
          onChange: saveFollowEtiqueta
        });
        
        var tagName = getOptionsPro('filterTag_favoritos');
        if (typeof tagName !== 'undefined' && tagName != '') {
            setTimeout(function(){ 
                $('.tableFavoritos .tagTableText_'+tagName).eq(0).trigger('click');
                console.log('tagName',tagName);
            }, 500);
        }
        initPanelResize('#favoritesProDiv .tabelaPanelScroll', 'favoritesPro');

        tableFavorites.tablesorter({
            textExtraction: {
                2: function (elem, table, cellIndex) {
                  var target = $(elem).find('.dateboxDisplay').eq(0);
                  var text_date = target.data('time-sorter');
                  return text_date;
                },
                7: function (elem, table, cellIndex) {
                    var target = parseInt($(elem).data('order'));
                    return target;
                }
              },
              widgets: ["saveSort", "filter"],
              widgetOptions: {
                  saveSort: true,
                  filter_hideFilters: true,
                  filter_columnFilters: true,
                  filter_saveFilters: true,
                  filter_hideEmpty: true,
                  filter_excludeFilter: {}
              },
              sortReset: true,
              headers: {
                  0: { sorter: false, filter: false },
                  1: { filter: true },
                  2: { filter: true },
                  3: { filter: true },
                  4: { filter: true }
              }
        }).on("sortEnd", function (event, data) {
            checkboxRangerSelectShift(idTableFavorite);
        }).on("filterEnd", function (event, data) {
            checkboxRangerSelectShift(idTableFavorite);
            var caption = $(this).find("caption").eq(0);
            var tx = caption.text();
                caption.text(tx.replace(/\d+/g, data.filteredRows));
                $(this).find("tbody > tr:visible > td > input").prop('disabled', false);
                $(this).find("tbody > tr:hidden > td > input").prop('disabled', true);
        });
        checkboxRangerSelectShift(idTableFavorite);

        tableFavorites.sortable({
            items: 'tr',
            cursor: 'grabbing',
            handle: '.sorterTrFavorite',
            forceHelperSize: true,
            opacity: 0.5,
            axis: 'y',
            dropOnEmpty: false,
            update: function(event, ui) {
                console.log(event, ui);
                setTimeout(function(){ 
                    var storeFavorites = getStoreFavoritePro();
                    $('#favoriteTablePro').find('tbody tr').each(function(index, value){
                        var _tr = $(this);
                        var id_procedimento = _tr.data('id_procedimento');
                        var favoriteIndex = storeFavorites.favorites.findIndex((obj => obj.id_procedimento == id_procedimento));

                        if (typeof favoriteIndex !== 'undefined' && favoriteIndex !== null) {
                                var newIndex = index+1;
                                var item = storeFavorites.favorites[favoriteIndex];
                                    item.order = newIndex;
                                storeFavorites.favorites[favoriteIndex] = item;
                        }
                    });
                    localStorageStorePro('configDataFavoritesPro', storeFavorites);
                    saveConfigFav();
                    setPanelFavorites('refresh');
                }, 500);
            }
        });

        var observerTableFav = new MutationObserver(function(mutations) {
            var _this = $(mutations[0].target);
            var _parent = _this.closest('table');
            var count_all = _parent.find('tr.infraTrMarcada').length;
            if (count_all > 0) {
                $('#favoritosProActions').find('.iconFavoritos_remove').show().find('.fa-layers-counter').text(count_all);
            } else {
                $('#favoritosProActions').find('.iconFavoritos_remove').hide();
            }
        });
        setTimeout(function(){ 
            tableFavorites.find('tbody tr').each(function(){
                observerTableFav.observe(this, {
                        attributes: true
                });
            });
            checkboxRangerSelectShift();
        }, 500);

        var filterFav = tableFavorites.find('.tablesorter-filter-row').get(0);
        if (typeof filterFav !== 'undefined') {
            var observerFilterTableFav = new MutationObserver(function(mutations) {
                var _this = $(mutations[0].target);
                var _parent = _this.closest('table');
                var iconFilter = _parent.find('.filterIfraTable');
                var checkIconFilter = iconFilter.hasClass('active');
                var hideme = _this.hasClass('hideme');
                if (hideme && checkIconFilter) {
                    iconFilter.removeClass('active');
                }
            });
            setTimeout(function(){ 
                var htmlFilterFav = '<div class="btn-group filterIfraTable" role="group" style="right: 30px; top: -15px;z-index: 999; position: absolute;">'+
                                    '   <button type="button" onclick="downloadTablePro(this)" data-icon="fas fa-download" style="padding: 0.1rem .5rem; font-size: 9pt;" data-value="Baixar" class="btn btn-sm btn-light">'+
                                    '       <i class="fas fa-download" style="padding-right: 3px; cursor: pointer; font-size: 10pt; color: #888;"></i>'+
                                    '       <span class="text">Baixar</span>'+
                                    '   </button>'+
                                    '   <button type="button" onclick="copyTablePro(this)" data-icon="fas fa-copy" style="padding: 0.1rem .5rem; font-size: 9pt;" data-value="Copiar" class="btn btn-sm btn-light">'+
                                    '       <i class="fas fa-copy" style="padding-right: 3px; cursor: pointer; font-size: 10pt; color: #888;"></i>'+
                                    '       <span class="text">Copiar</span>'+
                                    '   </button>'+
                                    '   <button type="button" onclick="filterIfraTable(this)" style="padding: 0.1rem .5rem; font-size: 9pt;" data-value="Pesquisar" class="btn btn-sm btn-light '+(tableFavorites.find('tr.tablesorter-filter-row').hasClass('hideme') ? '' : 'active')+'">'+
                                    '       <i class="fas fa-search" style="padding-right: 3px; cursor: pointer; font-size: 10pt;"></i>'+
                                    '       Pesquisar'+
                                    '   </button>'+
                                    '</div>';

                tableFavorites.find('thead .filterIfraTable').remove();
                tableFavorites.find('thead').prepend(htmlFilterFav);
                observerFilterTableFav.observe(filterFav, {
                    attributes: true
                });
            }, 500);
        }
    } else {
        setTimeout(function(){ 
            initFunctionsPanelFav(TimeOut - 100); 
            console.log('Reload initFunctionsPanelFav'); 
        }, 500);
    }
}
function actionToolbarFavPro(this_, triggerButton) {
    var button = $(triggerButton);
    var name_action = button.data('action');
    if (name_action == 'etiqueta') {
        showFollowEtiqueta(this_, 'show');
    } else if (name_action == 'remove') {
        removeFav(this_);
    } else if (name_action == 'dates') {
        showDatesFav(this_, 'show');
    } else if (name_action == 'descricao') {
        editFavoriteDesc(this_);
    }
}
function updateDatesFav(this_) {
    var storeFavorites = getStoreFavoritePro();
    var index = parseInt($(this_).closest('tr').data('index'));
    var config = getOptionsConfigDate(index);
    if ($(this_).val() != '') {
            if ($(this_).val() != config.date && config.date != '' && $(this_).val() != '' ) {
                config.selectdoc = false;
                config.setdate = true;
            }
            config.date = ($(this_).val() != '') ? $(this_).val() : config.date;
            config.dateTo = moment().format('YYYY-MM-DD');
        var htmlDatePreview = getDatesPreview(config);
        var followLink = $(this_).closest('td').find('.followLink')[0].outerHTML;
            //console.log('updateDatesFav',config);
            $(this_).closest('td').find('.info_dates_fav').html(htmlDatePreview+followLink);
            storeFavorites['favorites'][index]['configdate'] = config;
            localStorageStorePro('configDataFavoritesPro', storeFavorites);
    }
}
function showDatesFav(this_, mode) {
    if(!$(this_).closest('tr').find('.favoriteConfigDates').is(':hover')) {
        $(this_).closest('table').find('.info_dates_fav').show();
        $(this_).closest('table').find('.info_dates_fav_txt').hide();
        $(this_).closest('table').find('.followLinkDates').show();
        infraTooltipOcultar();
        updateDatesFav(this_);
    }
    if(mode == 'show') {
        $(this_).closest('td').find('.followLinkDates').hide();
        $(this_).closest('tr').find('.info_dates_fav').hide();
        $(this_).closest('tr').find('.info_dates_fav_txt').css('display','inline-flex').find('input.favoriteDatesPro').focus().trigger('click');
    }
    if ($(this_).closest('tr').find('.info_dates_fav').text().trim() != '') {
        $(this_).closest('td').removeClass('info_dates_follow_empty');
    } else {
        $(this_).closest('td').addClass('info_dates_follow_empty');
    }
}

function removeFav(this_) { 
    var storeFavorites = getStoreFavoritePro();
    var index = parseInt($(this_).closest('tr').data('index'));
    if (typeof index && parseInt(index) >= 0) {
        storeFavorites['favorites'].splice(parseInt(index),1);
        $(this_).closest('tr').trigger('click').effect('highlight').effect('highlight').fadeOut( "slow", function() {
            $(this).remove();
            updateIndexTableFav();
            updateCountTableFav();
            localStorageStorePro('configDataFavoritesPro', storeFavorites);
        });
    }
}
function updateIndexTableFav() {
    $('.tableFollow').find('tbody tr').each(function(index){
        $(this).data('index', index);
    });
}